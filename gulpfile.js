var gulp = require('gulp');
var uglify = require('gulp-uglify');
var server = require('gulp-server-livereload');
var gulpif = require('gulp-if');
var useref = require('gulp-useref');
var templateCache = require('gulp-angular-templatecache');
var htmlreplace = require('gulp-html-replace');
var gulpSequence = require('gulp-sequence');
var minifyCss = require('gulp-clean-css');

// Startet einen Webserver für die Entwicklung unter 'localhost:8888'.
gulp.task('webserver', function () {
    gulp.src(['app','bower_components'])
        .pipe(server({port: 8888, host: 'localhost'}));
});

// Startet einen Webserver für die Betrachtung des Produktiv-Builds unter 'localhost:9000'.
gulp.task('webserver:dist', function () {
    gulp.src('dist')
        .pipe(server({port: 9000, host: 'localhost'}));
});

// Kopiert den Ordner 'app/icons' nach 'app/dist/icons'.
gulp.task('copyIcons', function() {
    return gulp.src('app/icons/*.*')
        .pipe(gulp.dest('dist/icons/'))
});

// Kopiert den Ordner 'app/data' nach 'dist/data'.
gulp.task('copyData', function() {
    return gulp.src('app/data/*.*')
        .pipe(gulp.dest('dist/data/'))
});

// Erzeugt einen AngularJS Template-Cache für alle Templates im Projekt.
gulp.task('createTemplateCacheScript', function () {
    return gulp.src(['!app/index.html', '!app/bower_components/**/*.*', 'app/**/*.html'])
        .pipe(templateCache({module: 'dbfz'}))
        .pipe(gulp.dest('dist'));
});

// Bundelt alle Javascript-Dateien und minimiert diese.
gulp.task('resolveScriptDepsInIndexAndMinifyThem', function () {

    return gulp.src('app/index.html')
        .pipe(useref({ searchPath: ['app','bower_components'] }))
        .pipe(gulpif('*.directive.js', uglify()))
        .pipe(gulpif('*.controller.js', uglify()))
        .pipe(gulpif('*.service.js', uglify()))
        .pipe(gulpif('*.css', minifyCss()))
        .pipe(gulp.dest('dist'));

});

// Ersetzt die File-Referenzen in der index.html.
gulp.task('setTemplateScriptInIndex', function () {
    return gulp.src('dist/index.html')
        .pipe(htmlreplace({template: ['templates.js']}))
        .pipe(gulp.dest('dist/'));
});

// Baut das Projekt unter 'dist'.
gulp.task('build', gulpSequence(
    ['createTemplateCacheScript', 'resolveScriptDepsInIndexAndMinifyThem'],
    'setTemplateScriptInIndex',
    'copyIcons',
    'copyData'));