angular.module('dbfz', [
    'ngSanitize',
    'ngCsv',
    'ui.validate',
    'angularFileUpload'
]);
