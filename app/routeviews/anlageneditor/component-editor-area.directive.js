angular.module('dbfz').directive('componentEditorArea', function (CalculationParameters, Powerplant, GraphUtils) {

    return {
        restrict: 'E',
        scope: {
            editingItem: '=',
            contents: '='
        },
        templateUrl: 'routeviews/anlageneditor/component-editor-area.directive.html',
        link: function ($scope) {
            Powerplant.substrate.then(function (data) {
                $scope.substrate = data;
            });
            $scope.substratVerfahren = [
                {key:'rw',   name:'Richtwert'},
                {key:'fots', name:'FoTS'},
                {key:'fma',  name:'Futtermittelanalytik'},
                {key:'ea',   name:'Elementaranalyse'},
                {key:'bw',   name:'Brennwert'},
                {key:'csb',  name:'Chemischer Sauerstoffbedarf'},
                {key:'toc',  name:'Gesamter organischer Kohlenstoff'}];
            var verfahrenFns = {
                'rw': CalculationParameters.calcRichtwert,
                'fots': CalculationParameters.calcFoTS,
                'fma': CalculationParameters.calcFuttermittel,
                'ea':CalculationParameters.calcElementar,
                'bw':CalculationParameters.calcBrennwert,
                'csb':CalculationParameters.calcCSB,
                'toc':CalculationParameters.calcTOC
            };

            $scope.$watch('editingItem', function (newItem) {
                if (newItem){
                    switch(newItem.content_type){
                        case 'bhkw':
                            CalculationParameters.calcBhkw(newItem, Powerplant.sums, Powerplant.stochiometrie);
                            break;
                        case 'substrat':
                            var fn = verfahrenFns[newItem.verfahren.key];
                            if(fn) { fn.call(null, newItem, Powerplant.stochiometrie); }
                            break;
                    }
                }
            }, true);
        }
    };
});