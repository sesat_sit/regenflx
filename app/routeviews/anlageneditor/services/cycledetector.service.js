//////////////////////////////// Cycle Detection Functions /////////////////////////////////////////
angular.module('dbfz').service('CycleDetector', function () {

    //class node
    var Node = function ($id) {

        this.id = $id;
        this.marked = false;
        this.successors = [];
        this.predecessors = [];
        // used in tarjan algorithm
        this.index = -1;
        this.lowlink = -1;
        this.selfLoop = false;
    };

    Node.prototype = {
        equals: function (node) {
            // equality check based on vertex name
            return (node.id && this.id === node.id);
        }
    };

    //class graph
    var Graph = function ($scopeContents) {
        //node list of graph
        this.nodeList = [];
        this.leafs = [];
        this.scopeContents = $scopeContents;
        this.cycles = [];
    };

    Graph.prototype = {

        getNodeById: function (id) {
            for (var i in this.nodeList) {
                if (this.nodeList[i].id === id) {
                    return this.nodeList[i];
                }
            }
        },

        addEdge: function (sourceId, targetId) {
            var sourceNode = this.getNodeById(sourceId);
            var targetNode = this.getNodeById(targetId);
            sourceNode.successors.push(targetNode);
            targetNode.predecessors.push(sourceNode);
        },

        deleteEdge: function (sourceId, targetId) {
            var sourceNode = this.getNodeById(sourceId);
            var targetNode = this.getNodeById(targetId);
            for (var i = 0; i < sourceNode.successors.length; i++) {
                if (sourceNode.successors[i].id === targetId) {
                    sourceNode.successors.splice(i, 1);
                    break;
                }
            }
            for (i = 0; i < targetNode.predecessors.length; i++) {
                if (targetNode.predecessors[i].id === sourceId) {
                    targetNode.predecessors.splice(i, 1);
                    break;
                }
            }
        },

        //create graph structure
        createGraphStructure: function () {

            this.leafs = [];
            //list of possible element connectors (anchors)
            var connector = ['RightMiddle', 'LeftMiddle', 'TopCenter', 'BottomCenter', 'Unknown'];

            //create all nodes of graph by id
            for (var n = 0; n < this.scopeContents.length; n++) {

                this.nodeList[n] = new Node(this.scopeContents[n].id);

            }

            //fill successors and predecessors attrs
            for (n = 0; n < this.nodeList.length; n++) {
                //if there are no edges, this node mark as leaf
                if (this.scopeContents[n].edges.length === 0) {
                    this.leafs.push(this.nodeList[n]);
                    continue;
                }

                for (var edge in this.scopeContents[n].edges) {
                    if (this.scopeContents[n].edges.hasOwnProperty(edge)) {
                        var targetId = '';
                        for (var con in connector) {
                            if (connector.hasOwnProperty(con)) {
                                //search for target string in target attr
                                var position = this.scopeContents[n].edges[edge].target.search(connector[con]);

                                if (position > -1) {

                                    targetId = this.scopeContents[n].edges[edge].target.substring(0, position);
                                    //note successors and predecessors of target
                                    for (var tn in this.nodeList) {

                                        if (this.nodeList[tn].id === targetId) {
                                            this.nodeList[n].successors.push(this.nodeList[tn]);
                                            this.nodeList[tn].predecessors.push(this.nodeList[n]);
                                            break;
                                        }
                                    }
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        },

        resetMarked: function () {
            for (var i in this.nodeList) {
                this.nodeList[i].marked = false;
            }
        },

        //sort elements
        sortElements: function () {
            this.resetMarked();
            //ordered output List of elements by id
            var stack = [];

            //find and mark leafs
            for (var l in this.leafs) {
                this.leafs[l].marked = true;
                stack.push(this.leafs[l].id);
            }

            //recursive marking algorithm to order elements
            var rekOrder = function (node) {

                //breadth first search
                for (var pn in node.predecessors) {
                    //predecessor already marked => get next
                    if (node.predecessors[pn].marked) {
                        continue;
                    }
                    var markNewNode = true;
                    //get all successors of this predecessor and check marked tag
                    for (var sn in node.predecessors[pn].successors) {

                        if (node.predecessors[pn].successors[sn].marked === false) {
                            markNewNode = false;
                            break;
                        }

                    }
                    //if all successors has already been marked => mark this predecessor and push to output stack
                    if (markNewNode) {

                        node.predecessors[pn].marked = true;
                        stack.push(node.predecessors[pn].id);
                        //recursive call to marked predecessor
                        rekOrder(node.predecessors[pn]);
                    }
                }
            };

            //reorder elements in scope.contents
            var reorderContents = function (scopeContents, stack) {
                var debug = 0;
                for (var s in stack) {
                    if (stack.hasOwnProperty(s)) {
                        for (var cont in scopeContents) {
                            if (scopeContents.hasOwnProperty(cont)) {
                                if (scopeContents[cont].id === stack[s]) {
                                    scopeContents.push(scopeContents[cont]);
                                    debug++;
                                }
                            }
                        }
                    }
                }

                if (debug !== stack.length) alert('Sort error!');
                for (var d = 0; d < stack.length; d++) {
                    scopeContents.shift();
                }
            };

            //call recursive function for all leafs
            for (var leaf in this.leafs) {
                rekOrder(this.leafs[leaf]);
            }

            reorderContents(this.scopeContents, stack);
        },

        detectCycles: function () {

            this.cycles = [];
            var tarjan = new Tarjan(this);
            tarjan.run();

            //get cycles
            for (var scc in tarjan.strongConnectedComponents) {
                //cycle detection
                if (tarjan.strongConnectedComponents[scc].length === 1) { //node is connected to him self
                    if (tarjan.strongConnectedComponents[scc][0].selfLoop) {
                        this.cycles.push(tarjan.strongConnectedComponents[scc]);
                    }
                } else if (tarjan.strongConnectedComponents[scc].length > 1) { //cycle
                    /*
                              for( j in tarjan.strongConnectedComponents[scc]){
                                  if(tarjan.strongConnectedComponents[scc][j].selfLoop){
                                      this.cycles.push(new Array(tarjan.strongConnectedComponents[scc][j]));
                                  }
                              }*/
                    this.cycles.push(tarjan.strongConnectedComponents[scc]);
                }
            }
        },

        getCycleErrorString: function () {
            var alertString = 'Zyklen im Materialfluss erkannt:\n';
            for (var cycle in this.cycles) {

                for (var node in this.cycles[cycle]) {
                    if (this.cycles[cycle].hasOwnProperty(node)) {
                        alertString += this.cycles[cycle][node].id + '-->';
                    }
                }
                alertString += ';\n';
            }
            alertString += 'Bitte berichtigen Sie ihren Entwurf!';

            return alertString;
        }
    };

    var Tarjan = function (graph) {

        this.graph = graph;
        this.tarjanStack = [];
        this.strongConnectedComponents = [];
        this.index = 0;
    };

    Tarjan.prototype = {

        run: function () {
            this.reset();
            for (var i in this.graph.nodeList) {
                if (this.graph.nodeList.hasOwnProperty(i)) {
                    if (this.graph.nodeList[i].index < 0) {
                        this.strongconnect(this.graph.nodeList[i]);
                    }
                }
            }

            return this.strongConnectedComponents;
        },

        reset: function () {
            for (var i in this.graph.nodeList) {
                if (this.graph.nodeList.hasOwnProperty(i)) {

                    this.graph.nodeList[i].index = -1;
                    this.graph.nodeList[i].lowlink = -1;
                    this.graph.nodeList[i].selfLoop = false;
                }
            }
        },

        strongconnect: function (node) {
            // Set the depth index for v to the smallest unused index
            node.index = this.index;
            node.lowlink = this.index;
            this.index = this.index + 1;
            this.tarjanStack.push(node);
            // Consider successors of v
            // consider each node in node.successors
            for (var i in node.successors) {
                var v = node;
                var w = node.successors[i];
                //node is connected to him self
                if (node.successors[i].id === node.id) {
                    node.selfLoop = true;
                }
                if (w.index < 0) {
                    // Successor w has not yet been visited; recurse on it
                    this.strongconnect(w);
                    v.lowlink = Math.min(v.lowlink, w.lowlink);
                } else {
                    for (var t in this.tarjanStack) {
                        if (this.tarjanStack.hasOwnProperty(t)) {
                            if (this.tarjanStack[t].equals(w)) {
                                // Successor w is in stack S and hence in the current strong connected component
                                v.lowlink = Math.min(v.lowlink, w.index);
                                break;
                            }
                        }
                    }
                }
            }

            // If v is a root node, pop the stack and generate an strong connected component
            if (node.lowlink === node.index) {
                // start a new strongly connected component
                var nodes = [];
                w = null;
                if (this.tarjanStack.length > 0) {
                    do {
                        w = this.tarjanStack.pop();
                        // add w to current strongly connected component
                        nodes.push(w);
                    } while (!node.equals(w));
                }
                // output the current strongly connected component
                if (nodes.length > 0) {
                    this.strongConnectedComponents.push(nodes);
                }
            }
        }
    };

    this.examineContents = function (contents) {
        //create graph
        var graph = new Graph(contents);
        graph.createGraphStructure();
        //detect cycles
        graph.detectCycles();

        //sort if no cycles
        if (graph.cycles.length !== 0) {
            alert(graph.getCycleErrorString());//paste cycle in $scope.contents array, see above
        }
        return graph;
    };

    this.sortContents = function (contents) {
        var graph = new Graph(contents);
        graph.createGraphStructure();
        //detect cycles
        graph.detectCycles();
        graph.sortElements();
    }
});
