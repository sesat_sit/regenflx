////////////////////// Manipulation of the flow chart /////////////////////////////////////////////
angular.module('dbfz').service('GraphUtils', function () {

    this.findEdge = function (contents, sourceId, targetId) {
        for (var i = 0; i < contents.length; i++) {
            var obj = contents[i];
            for (var j = 0; j < obj.edges.length; j++) {
                var edge = obj.edges[j];
                if (obj.id === sourceId && edge.target.indexOf(targetId) === 0) {
                    return edge;
                }
            }
        }
        return null;
    };

    this.deleteEdge = function (contents, sourceId, targetId) {
        for (var i = 0; i < contents.length; i++) {
            var obj = contents[i];
            for (var j = 0; j < obj.edges.length; j++) {
                var edge = obj.edges[j];
                if (obj.id === sourceId && edge.target.indexOf(targetId) === 0) {
                    obj.edges.splice(j, 1);
                    break;
                }
            }
        }
    };

    this.addEdge = function (contents, connInfo) {
        var c = connInfo.connection;
        for (var s = 0; s < contents.length; s++) {
            if (connInfo.sourceId === contents[s].id) {//XXX
                var source = c.endpoints[0].anchor.type;
                var targetAnchor = (connInfo.dropEndpoint ? connInfo.dropEndpoint.anchor : c.endpoints[1].anchor);
                var target = c.target.id + targetAnchor.type;
                var edge = {
                    source: source,
                    target: target,
                    sourdeId: connInfo.sourceId,
                    type: c.scope,
                    flow: 100
                };
                contents[s].edges.push(edge);
                return edge;
            }
        }
    };

    this.findNodeById = function (contents, id) {
        for (var i = 0; i < contents.length; i++) {
            if (contents[i].id === id) {
                return contents[i];
            }
        }
        return "";
    };

    var startsWith = function (str1, str2) {
        return str1.slice(0, str2.length) === str2;
    };

    this.findTarget = function (contents, edge) {
        var targetName = edge.target;
        for (var c = 0; c < contents.length; c++) {
            if (startsWith(targetName, contents[c].id)) {
                return contents[c];
            }
        }
        return null;
    };
});