angular.module('dbfz').directive('substrateeinsatz', function(Powerplant, CalculationParameters) {

    return {
        restrict : 'E',
        scope: {},
        templateUrl: 'routeviews/anlageneditor/substrateeinsatz.directive.html',
        link: function($scope) {
            $scope.$watch(function(){ return Powerplant.sums}, function(sums){ $scope.sums = sums;}, true);
            $scope.$watch(Powerplant.getSubstrate, function(substrate){ $scope.substrate = substrate;}, true);
        }
    };
});