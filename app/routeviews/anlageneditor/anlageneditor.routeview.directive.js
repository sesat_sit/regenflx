angular.module('dbfz').directive('anlagenEditor', function ($timeout, $locale, GraphUtils, NumberFormatter, CycleDetector, CalculationParameters, Powerplant) {
    $locale.NUMBER_FORMATS.GROUP_SEP = '.';
    $locale.NUMBER_FORMATS.DECIMAL_SEP = ',';
    return {
        restrict: 'E',
        scope: {},
        templateUrl: 'routeviews/anlageneditor/anlageneditor.routeview.html',
        link: function ($scope) {
            jsPlumb.reset();

            $scope.contents = Powerplant.contents;
            $scope.operator = Powerplant.operator;
            $scope.editingItem = Powerplant.editingItem;
            $scope.volumeflow = {};
            $scope.stochiometrie = Powerplant.stochiometrie;
            $scope.$watch(Powerplant.getBHKW, function(bhkw){
                $scope.bhkw = bhkw;
            },true);
            //paste all missing gas flow edges from fermenter's to bhkw's  to graph
            $scope.makeupGasflow = function () {
                makeupGasflowFromTo('fermenter', 'bhkw');
            };

            var removeGasFlow = function () {
                var targets = [];

                //remove in graph
                for (var i = 0; i < $scope.contents.length; i++) {
                    if ($scope.contents[i].content_type === 'fermenter') {
                        for (var j = 0; j < $scope.contents[i].edges.length; j++) {
                            if ($scope.contents[i].edges[j].type === 'gas') {
                                var targetId = $scope.contents[i].edges[j].target.replace('LeftMiddle', '');
                            }

                            targets.push(targetId);
                            GraphUtils.deleteEdge($scope.contents, $scope.contents[i].id, targetId);
                        }
                    }
                }

                //remove in jsPlumb
                for (var t in targets) {
                    //get all connections with the given target, to detect the right ones for removing
                    var connections = jsPlumb.getConnections({target: targets[t], scope: 'gas'});
                    //redraw only once, after remove and add all necessary edges (avoids to redraw after each change of view)
                    jsPlumb.doWhileSuspended(function () {
                        //detach all connections, we has been found
                        for (var j = 0; j < connections.length; j++) {
                            jsPlumb.detach(connections[j]);
                        }
                    });
                }
            };

            //paste edges from all elements of given to type (from) to all elements of given type (to)
            //to == 'bhkw', from == 'fermenter'
            var makeupGasflowFromTo = function (from, to) {

                var sourceAnchor = 'TopCenter';

                for (var j = 0; j < $scope.contents.length; j++) {
                    if ($scope.contents[j].content_type === to) {
                        for (var i = 0; i < $scope.contents.length; i++) {
                            if ($scope.contents[i].content_type === from) {
                                var edge = {
                                    source: sourceAnchor,
                                    target: $scope.contents[j].id + 'LeftMiddle',
                                    sourdeId: $scope.contents[i].id,
                                    type: 'gas',
                                    flow: 100
                                };
                                $scope.contents[i].edges.push(edge);
                                var param = {
                                    uuids: [$scope.contents[i].id + sourceAnchor, $scope.contents[j].id + 'LeftMiddle'],
                                    editable: false
                                };
                                jsPlumb.connect(param);
                            }
                        }
                    }
                }
            };


            //////////////// Integration with JSPlumb Toolkit /////////////////////////////////////////////////////

            //push clicked edges on the top of svg, to render them last => dragging of this edge endpoint is now possible (usability?)
            jsPlumb.bind('click', function (conn) {

                //return for gasFlow cause connections crashed with 'Cannot read property 'offsetLeft' of null' error
                if (conn.scope === 'gas') {
                    return;
                }

                //set element form on right side of editor
                $scope.$apply(function () {
                    $scope.editItem(GraphUtils.findNodeById($scope.contents, conn.sourceId));
                });

                //get all connections with the given target, to detect the right ones for reordering
                var connections = jsPlumb.getConnections({target: conn.targetId, scope: '*'});

                //redraw only once, after remove and add all necessary edges (avoids to redraw after each change of view)
                jsPlumb.doWhileSuspended(function () {
                    //detach all connections, we has been found
                    for (var tc in connections) {
                        if (connections.hasOwnProperty(tc)) {
                            jsPlumb.detach(connections[tc]);
                        }
                    }
                    //find connection which should be in the top of svg element (rendered last) and build it
                    for (var t in connections) {
                        if (connections.hasOwnProperty(t) && connections[t] === conn) {
                            e = GraphUtils.findEdge($scope.contents, connections[t].sourceId, connections[t].targetId);
                            param = {
                                uuids: [e.sourceId + e.source, e.target],
                                editable: true
                            };
                            jsPlumb.connect(param);
                            break;
                        }
                    }
                    //build all over connections
                    for (var c in connections) {
                        if (connections.hasOwnProperty(c) && connections[c] !== conn) {
                            var e = GraphUtils.findEdge($scope.contents, connections[c].sourceId, connections[c].targetId);
                            var param = {
                                uuids: [e.sourceId + e.source, e.target],
                                editable: true
                            };
                            jsPlumb.connect(param);
                        }
                    }
                });
            });

            //push next edge on the top of svg, if endpoint has been clicked => dragging of this edge endpoint is now possible (usability?)
            jsPlumb.bind('endpointClick', function (endpoint) {
                //return for gasFlow cause connections crashed with 'Cannot read property 'offsetLeft' of null' error
                if (endpoint.scope === 'gas') {
                    return;
                }
                //get all connections with the given target, to detect the right ones for reordering
                var connections = jsPlumb.getConnections({target: endpoint.connections[0].targetId, scope: '*'});
                var conLength = connections.length;

                if (conLength <= 1) return;
                //redraw only once, after remove and add all necessary edges (avoids to redraw after each change of view)
                jsPlumb.doWhileSuspended(function () {
                    //detach all connections, we has been found
                    for (var tc in connections) {
                        if (connections.hasOwnProperty(tc)) {
                            jsPlumb.detach(connections[tc]);
                        }
                    }
                    //find connection which should be on top of svg element (rendered last) and build it
                    e = GraphUtils.findEdge($scope.contents, connections[conLength - 1].sourceId, connections[conLength - 1].targetId);
                    param = {
                        uuids: [e.sourceId + e.source, e.target],
                        editable: true
                    };
                    jsPlumb.connect(param);
                    //build all over connections
                    for (var c = 0; c <= conLength - 2; c++) {
                        var e = GraphUtils.findEdge($scope.contents, connections[c].sourceId, connections[c].targetId);
                        var param = {
                            uuids: [e.sourceId + e.source, e.target],
                            editable: true
                        };
                        jsPlumb.connect(param);
                    }
                });
            });

            //delete edge
            jsPlumb.bind('connectionDetached', function (conn, event) {
                if (event) {
                    $scope.$apply(function () {
                        GraphUtils.deleteEdge($scope.contents, conn.sourceId, conn.targetId);
                    });
                }
            });

            //establish new connection only if there is no cycle
            jsPlumb.bind('beforeDrop', function (connInfo) {
                // abort if is already another connection
                if (GraphUtils.findEdge($scope.contents, connInfo.sourceId, connInfo.targetId)) {
                    return false;
                }

                //detect cycle in Graph structure; add Edge if there is no cycle
                return $scope.$apply(function () {
                    //copy scope, take new edge and detect cycles
                    var contents = angular.copy($scope.contents);
                    GraphUtils.addEdge(contents, connInfo);
                    var g = CycleDetector.examineContents(contents);

                    return g.cycles.length === 0;
                })
            });

            jsPlumb.bind('connection', function (connInfo, originalEvent) {
                if (originalEvent) {
                    $scope.$apply(function () {
                        //add new edge only if added via jsplumb
                        GraphUtils.addEdge($scope.contents, connInfo);
                        CycleDetector.sortContents($scope.contents);
                    });
                }

                var edge = GraphUtils.findEdge($scope.contents, connInfo.sourceId, connInfo.targetId);
                var c = connInfo.connection;

                // change labels
                c.getOverlay('label').setLabel(NumberFormatter.roundNDecimalPlaces(edge.flow, 2) + 't');

                if (c.scope === 'gas') {
                    c.getOverlay('label').setVisible(false);
                }
            });

            //call for tab editor. after loading a new configuration file all connections are going to draw on one point,
            //so we have to repaint all elements of editor.
            $scope.repaint = function () {
                jsPlumb.repaintEverything();
            };

            //------------------Speichern/Laden--------------------------
            // show file info.
            $scope.fileName = 'Beispiel Konfiguration';
            $scope.fileLastModified = '-';
            $scope.url = 'content.json';


            //define download-file by contents array
            $scope.defineJsonSaveFile = function (scopeContents, operator) {

                var graph = CycleDetector.examineContents(scopeContents);
                if (graph.cycles.length > 0) {
                    $scope.json = encodeURI(graph.getCycleErrorString());
                    return;
                }
                var tempContents = angular.copy(scopeContents);

                //set style attribute (save layout)
                for (var i in tempContents) {
                    if (tempContents.hasOwnProperty(i)) {
                        var htmlElement = document.getElementById(tempContents[i].id);
                        tempContents[i].style = 'top: ' + htmlElement.style.top + '; left: ' + htmlElement.style.left + ';';
                    }
                }
                //add contents to output
                var jsonOutput = '{ \n\"contents\": [\n';
                var j = 0;
                for (var con in tempContents) {
                    //remove unnecessary attributes
                    if (tempContents.hasOwnProperty(con)) {
                        delete tempContents[con]['remove'];
                        delete tempContents[con]['$$hashKey'];

                        if (j !== 0) jsonOutput += ',\n';
                        jsonOutput += JSON.stringify(tempContents[con]);
                        j++;
                    }
                }
                jsonOutput += '\n],';
                //add operator definition to output
                jsonOutput += '\n\"operator\": \n';
                jsonOutput += JSON.stringify(operator);
                jsonOutput += '\n}';
                //create output file
                $scope.json = encodeURI(jsonOutput);
            };

            //load file
            $scope.handleFileSelect = function ($files) {
                if (window.File && window.FileReader && window.FileList && window.Blob) {
                    // Great success! All the File APIs are supported.
                } else {
                    alert('The File APIs are not fully supported in this browser.');
                }
                var f = $files[0];   // only one file necessary
                //if push cancel button in file chooser
                if (f === undefined)
                    return;

                var reader = new FileReader();
                // Callback to handle the file contents.
                reader.onload = function () {
                    //read file
                    var newScope = JSON.parse(reader.result);
                    var establishFile = true;

                    //proof contents and operator definition
                    if (!('contents' in newScope) || !('operator' in newScope)) {
                        alert('Inkonsistentes Dateiformat! ');
                        return;
                    }
                    //prove operator definition
                    var newOperatorDef = newScope.operator;
                    //TODO: prove operator definition here
                    //prove contents
                    var newScopeContent = newScope.contents;
                    for (var i in newScopeContent) {
                        if (newScopeContent.hasOwnProperty(i)) {
                            if (!('content_type' in newScopeContent[i])) {
                                establishFile = false;
                            }
                            if (!('id' in newScopeContent[i])) {
                                establishFile = false;
                            }
                            if (!('name' in newScopeContent[i])) {
                                establishFile = false;
                            }
                            if (!('edges' in newScopeContent[i])) {
                                establishFile = false;
                            }
                        }
                    }
                    if (!establishFile) {
                        alert('Inkonsistentes Dateiformat!');
                        return;
                    }
                    //commit change in scope to angular and reset jsPlumb
                    $scope.$apply(function () {
                        for (var con in newScopeContent) {
                            //add attributes
                            if (newScopeContent.hasOwnProperty(con)) {
                                newScopeContent[con]['remove'] = false;
                            }
                        }
                        //reset jsPlumb data model
                        jsPlumb.deleteEveryEndpoint();
                        //take loaded content
                        $scope.contents = Powerplant.contents = newScopeContent;
                        $scope.operator = newOperatorDef;
                        // show file info.
                        $scope.fileName = f.name;
                        $scope.fileLastModified = f.lastModifiedDate.toString().substring(0, 24);
                    });
                };
                // Read in the image file as a data URL.
                reader.readAsText(f);
                //reset the file uploader input element
                //otherwise you can not load the same file (same file name) again
                d3.select('#fileUpload').attr('type', '');
                d3.select('#fileUpload').attr('type', 'file');
            };

            $scope.addElement = function (type) {
                Powerplant.addElement(type);
            };

            $scope.removeElement = function (item) {
                jsPlumb.removeAllEndpoints(item.id);
                //remove from scope
                Powerplant.removeElement(item);
                if (item === $scope.editingItem) {
                    $scope.editingItem = Powerplant.editingItem = null;
                }
            };

            $scope.setStyleOfElement = function (content) {
                var element = GraphUtils.findNodeById($scope.contents, content.id);
                d3.select('#' + content.id).attr('style', element.style);
            };

            //select item <=> open form of element or diselect element
            $scope.editItem = function (item) {
                $scope.editingItem = item;
                Powerplant.editingItem = item;
            };

            //prepare element for remove or abort remove
            $scope.switchRemove = function (item) {
                //do not remove the last bhkw
                if (item.content_type === 'bhkw' && this.findOfType(contents, 'bhkw').length <= 1)
                    return;
                item.remove = !item.remove;
            };

            $scope.repaint();
        }
    };
});