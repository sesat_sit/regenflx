angular.module('dbfz').directive('effizienzbewertung', function(Powerplant, CalculationParameters) {

    return {
        restrict: 'E',
        scope: {},
        templateUrl: 'routeviews/anlageneditor/effizienzbewertung.directive.html',
        link: function ($scope) {
            $scope.efficiency = [
                {name: 'Methanvolumenstrom', calculated: {}},
                {name: 'BHKW-Arbeit', calculated: {}},
                {name: 'Restgaspotential', calculated: {}},
                {name: 'oTS Gärrest', calculated: {}},
                {name: 'TS Gärrest', calculated: {}}];

            function recalculateEfficiencies(args){
                var sums = args.sums, stoch=args.stoch, bhkw = args.bhkw, tmp = sums.ts*sums.ots*sums.fots;
                // Methanertrag
                $scope.efficiency[0].calculated.umsatz = bhkw.calculated.methanertrag*100000000/(sums.fm*sums.ts*sums.ots*sums.fots*stoch.methanpotential);
                // BHKW
                $scope.efficiency[1].calculated.umsatz = bhkw.calculated.vch4/(sums.fm*sums.ts*sums.ots*sums.methanpotential/10000)*100;
                // Restgaspotential
                var restgaspotential = args.gaerrest.parameters.yg;
                $scope.efficiency[2].calculated.umsatz = (tmp*stoch.methanpotential*100-restgaspotential*100000000)/(tmp*(stoch.methanpotential-restgaspotential*(1-stoch.wachstum/100+stoch.wasser/100)));
                // oTS Gaerrest
                var gaerrestots = args.gaerrest.parameters.ots;
                $scope.efficiency[3].calculated.umsatz = 100000000/(sums.ots*sums.fots*(100-stoch.wachstum))*(1-(100-sums.ots)/(100-gaerrestots));
                // TS Gaerrest
                var gaerrestts = args.gaerrest.parameters.ts;
                $scope.efficiency[4].calculated.umsatz = 100000000/(sums.ots*sums.fots*(100-stoch.wachstum-gaerrestts*(1-stoch.wachstum/100+stoch.wasser/100)))*(1-gaerrestts/sums.ts);
                $scope.efficiency.forEach(function(obj){
                    var e=obj.calculated;
                    if(e.umsatz <= 100) {
                        e.methanertrag = sums.fm * tmp * stoch.methanpotential * e.umsatz / 100000000;
                        e.restgaspotential = tmp / 1000000 * (1 - e.umsatz / 100) * stoch.methanpotential / (1 - (tmp / 1000000 * e.umsatz / 100 * (1 - stoch.wachstum / 100 + stoch.wasser / 100)));
                        e.ts = (sums.ts / 100 - tmp * e.umsatz / 100000000 * (1 - stoch.wachstum / 100)) / (1 - tmp * e.umsatz / 100000000 * (1 - stoch.wachstum / 100 + stoch.wasser / 100)) * 100;
                        e.ots = (sums.ots - sums.ots * sums.fots * e.umsatz / 1000000 * (100 - stoch.wachstum)) / (1 - sums.ots * sums.fots * e.umsatz / 100000000 * (100 - stoch.wachstum));
                    }else{
                        e.methanertrag = e.restgaspotential = e.ts = e.ots = null;
                    }
                });
            };
            $scope.$watch(function () {
                    return {
                        stoch: Powerplant.stochiometrie,
                        bhkw: Powerplant.getBHKW(),
                        gaerrest: Powerplant.getGaerrest(),
                        sums: Powerplant.sums
                    }
                },
                function (args) {
                    recalculateEfficiencies(args);
                }, true);
        }
    };
});