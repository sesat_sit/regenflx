angular.module('dbfz').directive('anlageninformation', function(Powerplant, CalculationParameters) {

    return {
        restrict : 'E',
        scope: {},
        templateUrl: 'routeviews/anlageneditor/anlageninformation.directive.html',
        link: function($scope) {


            $scope.stats = {};

            $scope.$watch(allStats,function(newStats){
                $scope.stats = newStats;
            }, true);

            function sumOf(arr){
                return arr.reduce(function (sum, v) { return sum + v }, 0);
            }

            function mapn(func){
                var arrays = Array.prototype.slice.call(arguments,1);
                var res = [];
                for (var i = 0; i < arrays[0].length; i++) {
                    var args = arrays.map(function(arr){return arr[i]});
                    res.push(func.apply(null,args));
                }
                return res;
            }

            function allStats() {
                var sumsFM = Powerplant.getSubstrate().map(function (s) {
                    return s.parameters.fm;
                });
                var tsPerSubstrat = Powerplant.getSubstrate().map(function(s){ return s.parameters.ts});
                var xaPerSubstrat = Powerplant.getSubstrate().map(function(s){ return s.parameters.xa});
                var fotsPerSubstrat = Powerplant.getSubstrate().map(function(s){ return s.parameters.FoTS});

                return {
                    sumFM: sumOf(sumsFM),
                    sumTS: sumOf(mapn(function(fm,ts){return fm*ts/100},sumsFM,tsPerSubstrat)),
                    sumOTS: sumOf(mapn(function(fm,ts,xa){return fm*ts*(1000-xa)/100000},sumsFM,tsPerSubstrat, xaPerSubstrat)),
                    sumFoTS: sumOf(mapn(function(fm,ts,fots){return fm*ts*fots/100000},sumsFM,tsPerSubstrat, fotsPerSubstrat)),
                    sumVolume: sumOf(CalculationParameters.getVfArray())
                }
            }

        }
    };
});