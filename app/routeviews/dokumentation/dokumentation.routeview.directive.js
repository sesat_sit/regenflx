angular.module('dbfz')
    .directive('dokumentation', function () {

        return {
            restrict: 'E',
            scope: {},
            templateUrl: 'routeviews/dokumentation/dokumentation.routeview.html',
            link: function ($scope) {

            }
        };
    });