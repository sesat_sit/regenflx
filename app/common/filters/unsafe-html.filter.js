angular.module('dbfz')
    .filter('unsafeHtml', function ($sce) {
        return function (state) {
            return $sce.trustAsHtml(state);
        }
    });