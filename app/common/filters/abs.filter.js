angular.module('dbfz')
    .filter('abs', function () {
        return function (input) {
            return Math.abs(input);
        }
    });