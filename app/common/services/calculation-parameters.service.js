angular.module('dbfz').service('CalculationParameters', function (Powerplant, GraphUtils) {

    var that = this;
    var hsch4=39.8;
    function ensureCalculated(item){
        if(!item.calculated) { item.calculated = {}};
    }
    function deriveFromFoTS(item,stoch){
        item.calculated.methanpotential = item.calculated.fots * stoch.methanpotential / 100;
        item.calculated.brennwert = item.calculated.methanpotential * 39.8;
    }
    function deriveFromMethanpotential(item,stoch){
        item.calculated.fots = item.calculated.methanpotential / stoch.methanpotential * 100;
        item.calculated.brennwert = item.calculated.methanpotential * 39.8;
    }
    function deriveFromBrennwert(item,stoch){
        item.calculated.fots = item.calculated.methanpotential / stoch.methanpotential * 100;
        item.calculated.methanpotential = item.calculated.brennwert / 39.8;
    }
    this.calcRichtwert = function(item,stoch){
        ensureCalculated(item);
        item.calculated.methanpotential = item.parameters.richtwert.vch4;
        deriveFromMethanpotential(item,stoch);
    };
    this.calcFoTS = function(item, stoch){
        ensureCalculated(item);
        if(item.parameters.fots.xf){
            var p=item.parameters.fots;
            var xf=item.parameters.fots.xf;
            var ots=item.parameters.ots;
            var fots = 10*(p['A']-p['B']*(100-ots)*10-p['C']*xf-p['D']*xf*xf)/ots;
            if(fots >= 0 && fots <= 1000) {
                item.calculated.fots = fots;
                item.parameters.fots.fots = item.calculated.fots;
            }else {
                item.calculated.fots = null;
                item.parameters.fots.fots = null;
            }
        }else if(item.parameters.fots.fots) {
            item.calculated.fots = item.parameters.fots.fots;
        }
        deriveFromFoTS(item,stoch);
    };
    this.calcFuttermittel = function(item, stoch){
        ensureCalculated(item);
        var ots=item.parameters.ots;
        var p = item.parameters.futtermittel;
        var fxc=(p.xf*p.fqxf+(10*ots-p.xf-p.xp-p.xl)*p.fqnfe)/100;
        var fxp=p.xp*p.fqxp/100;
        var fxl=p.xl*p.fqxl/100;
        item.calculated.methanpotential=(402*fxc+401*fxp+958*fxl)/(10*ots);
        deriveFromMethanpotential(item,stoch);
    };
    this.calcBrennwert = function(item, stoch){
        ensureCalculated(item);
        var p = item.parameters.brennwert;
        var hsots = p.hs;
        var fq = p.fq;
        var hsnfots = p.hsnfots;
        var fx = stoch.wachstum;
        item.calculated.brennwert = (hsots-(100-fq)*hsnfots/100)*(100-fx)/100;
        deriveFromBrennwert(item,stoch);
    };
    this.calcElementar = function(item, stoch){
        ensureCalculated(item);
        var p = item.parameters.elementar;
        var mc=12.0107,mh=1.0079,mo=15.9994,mn=14.0067,ms=32.065,vnorm=22.4141;
        item.calculated.methanpotential=(p.c/2+p.h/8-p.o/4-3*p.n/8-p.s/4)*vnorm*p.fq*(100-stoch.wachstum)/(10*(p.c*mc+p.h*mh+p.o*mo+p.n*mn+p.s*ms));
        deriveFromMethanpotential(item,stoch);
    };
    this.calcCSB = function(item, stoch){
        ensureCalculated(item);
        var p = item.parameters, csb=p.csb;
        item.calculated.methanpotential = (csb.csb*350.23*csb.fq*(100-stoch.wachstum))/(p.rho*p.ts*p.ots*1000);
        deriveFromMethanpotential(item,stoch);
    };
    this.calcTOC = function(item, stoch){
        ensureCalculated(item);
        var p = item.parameters, toc = p.toc;
        item.calculated.methanpotential = (toc.toc*1.8662*toc.cch4*toc.fq*(100-stoch.wachstum))/(10000*p.ots);
        deriveFromMethanpotential(item,stoch);
    };

    // BHKW
    this.calcBhkw = function(item, sums, stoch){
        ensureCalculated(item);
        if(item.parameters.calcType==='bhkw'){
            item.name='BHKW';
        }else{
            item.name='Gas';
        }
        var p = item.parameters;
        var c = item.calculated;
        c.methanertrag = p.trocken ? that.gasVolumeDry(p) : that.gasVolumeWet(p);
        var substratenergie = sums.fm*sums.ts*sums.ots*sums.brennwert/10000;
        if(p.calcTypeDetail == 'surplus'){
            c.welbrutto=p.welnutz/(100-p.tv)*100+p.weleigen;
            c.welnetto = p.welnutz;
        }else{
            c.welbrutto= p.welnutz/(100-p.tv)*100;
            c.welnetto = p.welnutz - p.weleigen;
        }
        c.ps = substratenergie/24/3.6+p.vzo*9.069/(p.tn*24);

        c.pfwl = c.welbrutto / (p.etael * p.bs) * 100;
        c.vch4 = (c.pfwl*p.bs-9.069*p.vzo)/(9.97*p.tn);
        c.k = (p.pn+p.qn)/c.ps; // Kapazitaetszahl

        if(c.welnetto!=null) {
            c.na = (c.welnetto+p.qnutz)/((p.pn+p.qn)*p.tn*24);//Arbeitsausnutzung
            c.omega = (c.welnetto+p.qnutz)/(c.ps*p.tn*24); // Brennstoffausnutzungsgrad
        }else{
            c.omega = null;
            c.na = null;
        }
    };


    //Nutzvolumen i.N. (feuchtes Biogas):
    this.gasVolumeDry = function (item) {
        //T_norm=273.15; P_norm = 1.01325
        return (item.methanertrag * item.gasdruck * 273.15) / (1.01325 * (item.gastemperatur + 273.15));
    };

    //Nutzvolumen i.N. (trockenes Biogas):
    this.gasVolumeWet = function (item) {
        //const A= 7.19621, B= 1730.63, C= 233.426;
        var p_w = Math.pow(10, 7.19621 - (1730 / (233.426 + item.gastemperatur))) / 100;
        return item.methanertrag * (item.gasdruck - p_w) * 273.15 / (1.01325 * (item.gastemperatur + 273.15));
    };

    function getDsArray() {
        var substrate = Powerplant.getSubstrate();
        var fermenter = Powerplant.getFermenter();
        var arr = [];
        for (var i = 0; i < substrate.length; i++) {
            var row = [];
            var subs = substrate[i];

            for (var j = 0; j < fermenter.length; j++) {
                var ferm = fermenter[j];
                var edge = subs.edges.filter(function (e) {
                    return e.target.indexOf(ferm.id) === 0
                })[0];
                row.push(edge ? edge.flow : 0);
            }
            arr.push(row);
        }
        return arr;
    };

    /* export array of arrays of weights for the mass flow of fermenter to fermenter
     The order is determined by the order in contents.
     */
    function getDfArray() {
        var fermenter = Powerplant.getFermenter();
        var arr = [];
        for (var i = 0; i < fermenter.length; i++) {
            var row = [];
            var ferm1 = fermenter[i];
            var ferm1Edges = ferm1.edges.filter(function (e) {
                var target = GraphUtils.findTarget(fermenter, e);
                return target && target.content_type === 'fermenter';
            });
            for (var j = 0; j < fermenter.length; j++) {
                var ferm2 = fermenter[j];
                if (i === j) {
                    row.push(0);
                } else {
                    var edge = ferm1Edges.filter(function (e) {
                        return e.target.indexOf(ferm2.id) === 0
                    })[0];
                    row.push(edge ? edge.flow : 0);
                }
            }
            arr.push(row);
        }
        return arr;
    };


    this.getVfArray = function() {
        var arr = [];
        var fermenter = Powerplant.getFermenter();
        for (var i = 0; i < fermenter.length; i++) {
            arr.push(fermenter[i].massvolume);
        }
        return arr;
    };

    function getdGRLArray() {
        var fermenter = Powerplant.getFermenter();
        var arr = [];
        for (var i = 0; i < fermenter.length; i++) {
            fermenter[i].edges.forEach(function (e) {
                var target = GraphUtils.findTarget(Powerplant.contents, e);
                if (target && target.content_type === 'rest') {
                    arr.push(e.flow);
                }
            });
            if(arr.length<=i){
                arr.push(0);
            }
        }
        return arr;
    }
    this.createParametersForCalculations = function () {
        var substrate = Powerplant.getSubstrate();
        var xFOTS = substrate.map(function(s){
            return s.parameters.ts*s.parameters.ots*s.calculated.fots/1000000;
        });
        var pS = substrate.map(function(s){
            return [s.parameters.k1,s.parameters.k2,s.parameters.alpha];
        });
        return {
            V_F: this.getVfArray(),
            x_FOTS: xFOTS,
            d_S: getDsArray(),
            d_F: getDfArray(),
            d_GRL: getdGRLArray(),
            p_S: pS,
            Y_ch4: Powerplant.stochiometrie.methanpotential

        };
    };
    function array(n,value){
        var arr = new Array(n);
        for(var i=0;i<n;i++){ arr[i]=value;}
        return arr;
    }
    function transpose(matrix) {
        var res = [];
        var rows = matrix.length;
        if(rows > 0) {
            var cols = matrix[0].length;

            for (let j = 0; j < cols; j++) {
                res[j] = Array(rows);
            }
            for (let i = 0; i < rows; i++) {
                for (let j = 0; j < cols; j++) {
                    res[j][i] = matrix[i][j];
                }
            }
        }
        return res;
    }
    this.calcProduction = function(args){
        var num_S = args.d_S.length, num_F = args.d_F.length;
        var q_out = [], V_ch4 = array(num_F, 0);
        if(num_S > 0 && num_F > 0){
            var A_1 = transpose(args.d_F), A_2 = transpose(args.d_F);
            var B_1 = array(num_F, 0), B_2 = array(num_F, 0);
            var C1 = array(num_S, 0), C2 = array(num_S, 0);
            // sum of each row in d_F+d_GRL
            for (var i = 0; i < num_F; i++) {
                var sum = args.d_GRL[i];
                var row = args.d_F[i];
                for (var j = 0; j < row.length; j++) {
                    sum += row[j];
                }
                q_out.push(sum);
            }
            for (var i = 0; i < num_S; i++) {
                for (var j = 0; j < num_F; j++) {
                    B_1[j] = -args.d_S[i][j] * args.x_FOTS[i] * args.p_S[i][2] / 100;
                    B_2[j] = -args.d_S[i][j] * args.x_FOTS[i] * (1 - args.p_S[i][2] / 100);
                    A_1[j][j] = -(q_out[j] + args.p_S[i][0] * args.V_F[j]);
                    A_2[j][j] = -(q_out[j] + args.p_S[i][1] * args.V_F[j]);
                }
                C1[i] = linear.solve(A_1, B_1);
                C2[i] = linear.solve(A_2, B_2);
            }
            for (var j = 0; j < num_F; j++) {
                for (var i = 0; i < num_S; i++) {
                    V_ch4[j] += (C1[i][j] * args.p_S[i][0] + C2[i][j] * args.p_S[i][1]) * args.V_F[j] * args.Y_ch4;
                }
            }
        }
        return V_ch4;
    };
});
