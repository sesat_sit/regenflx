angular.module('dbfz').service('Powerplant', function ($q, $http) {

    // load substrate parameters from csv
    var substrateMap;
    var service = this;
    service.stochiometrie = {wachstum: 5, wasser: 11.25, methan: 52};
    service.sums = {fm: 0, ts: 0, ots: 0, fots: 0, methanpotential: 0, brennwert: 0};
    // if the CSV was written with a german version of Excel we have commas instead of points as decimal separators
    function str2Number(s){
        return +s.replace(',','.');
    }

    service.substrate = $http.get('data/substrate.csv').then(function (text) {
        // parse csv
        var rows = d3.dsv(';', 'text/plain').parse(text.data);
        // create array of substrates
        var subs = rows.map(function (row) {
            var newRow = {
                name: row['Substrat'],
                ts: str2Number(row['TS']),
                ots: str2Number(row['oTS']),
                rho: str2Number(row['rho']),
                k1: str2Number(row['k1']),
                k2: str2Number(row['k2']),
                alpha: str2Number(row['alpha']),
                richtwert: {vch4: str2Number(row['Methanpotential'])},
                fots: {fots: str2Number(row['FoTS']),
                       A: str2Number(row['A']),
                       B: str2Number(row['B']),
                       C: str2Number(row['C']),
                       D: str2Number(row['D'])},
                futtermittel: {xf:str2Number(row['XF']),
                               xp:str2Number(row['XP']),
                               xl:str2Number(row['XL']),
                               fqxf:str2Number(row['FQ_XF']),
                               fqnfe:str2Number(row['FQ_NfE']),
                               fqxp:str2Number(row['FQ_XP']),
                               fqxl:str2Number(row['FQ_XL'])},
                elementar:{c:str2Number(row['C-Atome']),
                           h:str2Number(row['H-Atome']),
                           o:str2Number(row['O-Atome']),
                           n:str2Number(row['N-Atome']),
                           s:str2Number(row['S-Atome']),
                           fq:str2Number(row['FQ'])},
                brennwert: {hs:str2Number(row['H_S']),
                            fq:str2Number(row['FQ_Brennwert']),
                            hsnfots:25600},
                csb: {csb:str2Number(row['CSB']),
                      fq:str2Number(row['FQ_CSB'])},
                toc: {toc:str2Number(row['TOC']),
                      fq:str2Number(row['FQ_TOC']),
                      cch4:str2Number(row['c_CH4'])}
            };
            return newRow;
        });
        contentDefaults['substrat'] = {
            'content_type': 'substrat',
            'id': '',
            'fm': 5,
            'edges': [],
            'parameters': subs[0],
            'calculated': {},
            'verfahren': {key:'rw',   name:'Richtwert'},
            'style': ''
        };
        // map of substrate name to substrate
        substrateMap = subs.reduce(function (m, entry) {
            m[entry.name] = entry;
            return m;
        }, {});

        // add three demo substrates to the current model
        service.contents.push(
            {
                'id': 'subs1',
                'content_type': 'substrat',
                'fm': 8.0,
                'parameters': substrateMap['Maissilage'],
                'verfahren': {key:'rw',   name:'Richtwert'},
                'calculated': {
                    'methanpotential': 360,
                    'fots': 85.6572647457429,
                    'brennwert': 14327.999999999998
                },
                'edges': [{
                    'source': 'RightMiddle',
                    'target': 'ferm1LeftMiddle',
                    'sourceId': 'subs1',
                    'type': 'mass',
                    'flow': 6
                },
                    {
                        'source': 'RightMiddle',
                        'target': 'ferm2LeftMiddle',
                        'sourceId': 'subs1',
                        'type': 'mass',
                        'flow': 2
                    }],
                'style': 'top: 16px; left: 15px;'
            }, {
                'id': 'subs2',
                'content_type': 'substrat',
                'fm': 4,
                'parameters': substrateMap['Rindergülle'],
                'verfahren': {key:'rw',   name:'Richtwert'},
                'calculated': {
                    'methanpotential': 210,
                    'fots': 49.96673776835003,
                    'brennwert': 8358
                },
                'edges': [{
                    'source': 'RightMiddle',
                    'target': 'ferm2LeftMiddle',
                    'sourceId': 'subs2',
                    'type': 'mass',
                    'flow': 2
                },
                    {
                        'source': 'RightMiddle',
                        'target': 'ferm1LeftMiddle',
                        'sourceId': 'subs2',
                        'type': 'mass',
                        'flow': 2
                    }],
                'style': 'top: 330px; left: 15px;'
            });
        return subs;
    });

    var contentDefaults = {
        'rest': {
            'content_type': 'rest',
            'name': 'Gaerrest',
            'id': '',
            'edges': []
        },
        'fermenter': {
            'content_type': 'fermenter',
            'name': 'Fermenter',
            'id': '',
            'massvolume': 1000,
            'moisture': 'wet',
            'gasvolume': 100,
            'temperature': 38,
            'pressure': 1.01325,
            'edges': []
        },
        'substrat': {
            'content_type': 'substrat',
            'id': '',
            'fm': 5,
            'edges': [],
            'parameters': {},
            'calculated': {},
            'style': ''
        }
    };

    service.createNewElement = function (type) {
        var element = angular.copy(contentDefaults[type]);
        
        if (type !== 'substrate') {
            return element;
        } else {
            console.log(s);
            s.parameters = service.substrate[0];
        }
    };

    service.addElement = function (type) {
        var element = service.createNewElement(type);
        //only one bhkw and gasspeicher
        if (type === 'bhkw' && service.getBHKW().length > 0) {
            alert('Es kann maximal ein BHKW geben.');
            return;
        }
        //compute id for new element
        //set individual id and remove flag
        element.id = type + Math.floor((Math.random() * 100000) + 1);
        element.remove = false;
        service.contents.push(element);
    };

    service.removeElement = function (item) {
        // delete all edges to service.item
        for (var idx = 0; idx < service.contents.length; idx++) {
            var obj = service.contents[idx];
            for (var j = 0; j < obj.edges; j++) {
                var edge = obj.edges[j];
                if (edge.target.indexOf(item.id) === 0) {
                    obj.edges.splice(j, 1);
                    break;
                }
            }
        }
        service.contents.splice(service.contents.indexOf(item), 1);
    };

    //add attributes for plant definition to scope
    service.operator = {
        'name': 'Musterbetreiber',
        'address': 'Am Musterweg 21, 01234 Musterstadt',
        'mail': 'max.mustermann@anbieter.de',
        'phone': '0178 000 000'
    };

    service.editingItem = null;

    /////////// Main Model ////////////////////////////////////////////////
    service.contents = [
        {
            'content_type': 'bhkw',
            'name': 'BHKW',
            'id': 'bhkw59065',
            'edges': [],
            'parameters': {
                'tn': 5,
                'bs': 100,
                'pn': 220,
                'etael': 38.5,
                'qn': 180,
                'welnutz': 17500,
                'qnutz': 18500,
                'tv': 2,
                'weleigen': 1750,
                'vzo': 57.9,
                'methanertrag': 1155,
                'gasdruck': 1.01325,
                'gastemperatur': 38,
                'trocken': true,
                'calcType': 'bhkw',
                'calcTypeDetail': 'surplus'
            },
            'calculated': {
                'methanertrag': 1013.9426321709788,
                'welbrutto': 19607.14285714286,
                'welnetto': 17500,
                'ps': 492.70089749999977,
                'pfwl': 509.2764378478664,
                'vch4': 1011.0842263748574,
                'k': 0.8118515757321108,
                'na': 0.75,
                'omega': 0.608888681799083
            },
            'style': 'top: 26px; left: 565px;'
        },
        {
            'content_type': 'rest',
            'name': 'Gaerrest',
            'parameters': {yg:5.5, ts:8.3, ots:76.5},
            'id': 'rest75416',
            'edges': [],
            'style': 'top: 135px; left: 455px;'
        },
        {
            'content_type': 'fermenter',
            'name': 'Nachgärer',
            'id': 'ferm3',
            'massvolume': 400,
            'moisture': 'wet',
            'temperature': 38,
            'pressure': 1.01325,
            'edges': [{
                    'source': 'RightMiddle',
                    'target': 'rest75416LeftMiddle',
                    'sourceId': 'ferm3',
                    'type': 'mass',
                    'flow': 8
                }],
            'style': 'top: 262px; left: 492px;'
        },
        {
            'content_type': 'fermenter',
            'name': 'Fermenter 2',
            'id': 'ferm2',
            'massvolume': 400,
            'moisture': 'wet',
            'temperature': 38,
            'pressure': 1.01325,
            'edges': [{
                    'source': 'RightMiddle',
                    'target': 'ferm3LeftMiddle',
                    'sourceId': 'ferm2',
                    'type': 'mass',
                    'flow': 8
                }],
            'style': 'top: 322px; left: 282px;'
        },
        {
            'content_type': 'fermenter',
            'name': 'Fermenter 1',
            'id': 'ferm1',
            'massvolume': 400,
            'moisture': 'wet',
            'temperature': 38,
            'pressure': 1.01325,
            'edges': [{
                'source': 'RightMiddle',
                'target': 'ferm2LeftMiddle',
                'sourceId': 'ferm1',
                'type': 'mass',
                'flow': 4
            },
                {
                    'source': 'RightMiddle',
                    'target': 'ferm3LeftMiddle',
                    'sourceId': 'ferm1',
                    'type': 'mass',
                    'flow': 4
                }],
            'style': 'top: 92px; left: 216px;'
        }];


    service.findOfType = function (contents, type) {
        return contents.filter(function (c) {
            return c.content_type === type
        })
    };

    service.getSubstrate = function () {
        return service.findOfType(service.contents, 'substrat');
    };

    service.getGaerrest = function() {
        return service.findOfType(service.contents, 'rest')[0];
    };

    service.getFermenter = function () {
        return service.findOfType(service.contents, 'fermenter');
    };

    service.getBHKW = function () { // XXX assumes service there is exactly one BHKW, ugly...
        return service.findOfType(service.contents, 'bhkw')[0];
    };

    service.getBHKWPower = function () {
        return service.getBHKW().parameters.power;
    };
    service.recalculateSums = function(){
        var substrate = service.getSubstrate();
        var stoch = service.stochiometrie;
        var sums = service.sums = {fm: 0, ts: 0, ots: 0, fots: 0, methanpotential: 0, brennwert: 0};
        if(substrate  && substrate.length > 0) {
            for (var i = 0; i < substrate.length; i++) {
                var s = substrate[i];
                sums.fm += s.fm;
                sums.ts+=s.fm*s.parameters.ts;
                sums.ots+=s.fm*s.parameters.ts*s.parameters.ots;
                sums.fots+=s.fm*s.parameters.ts*s.parameters.ots*(s.calculated?s.calculated.fots:0);
            }
            sums.ts/=sums.fm;
            sums.ots/=(sums.fm*sums.ts);
            sums.fots/=(sums.fm*sums.ts*sums.ots);
            sums.methanpotential = sums.fots * stoch.methanpotential / 100;
            sums.brennwert = sums.methanpotential * 39.8;
        }
    }
});