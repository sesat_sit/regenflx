angular.module('dbfz').service('NumberFormatter', function () {

    //round float to n decimal places
    this.roundNDecimalPlaces = function (zahl, n) {
        var faktor = Math.pow(10, n);
        return (Math.round(zahl * faktor) / faktor);
    }
});
