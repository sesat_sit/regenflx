//directive adds collapsing button for the embedded content
angular.module('dbfz').directive('collapsable', function () {
    return {
        restrict: 'E',
        replace: true,
        transclude: true,
        scope: {
            title: '@', //title for expanding content (default no title)
            lines: '@', //show lines true or false (default true)
            bgcolor: '@', //background color for expanded content (default transparent)
            border: '@' //right and bottom border for expanded content (default false)
        },
        templateUrl: 'common/directives/collapsable/collapsable.directive.html',
        link: function (scope) {
            scope.isContentVisible = false;
            scope.toggleContent = function () {
                scope.isContentVisible = !scope.isContentVisible;
            };
        }
    };
});