// editor for edge flows
// Automatically adjusts input ranges, shows sum
angular.module('dbfz').directive('edgeFlow', function (GraphUtils, NumberFormatter) {
    return {
        scope: {
            item: '=',
            contents: '='
        },
        restrict: 'AE',
        templateUrl: 'common/directives/edge-flow/edge-flow.directive.html',
        link: function (scope) {

            scope.getSourceName = function () {

                if (!scope.item) {
                    return;
                }

                return scope.item.name;
            };

            scope.getTargetName = function (edge) {

                if (!scope.item || !scope.contents) {
                    return;
                }
                var node = GraphUtils.findTarget(scope.contents, edge);
                if (node) {
                    return node.name;
                } else {
                    return '???';
                }
            };

            scope.maximumLeft = calculateMaximum();

            function calculateMaximum() {
                var sum = scope.item.fm;
                if(sum === null || sum === undefined){
                    var fms=scope.item.fms;
                    if(!fms){
                        console.error("no input edges found, can't determine maximum mass flow for this node", scope.item);
                    }else{
                        sum = 0;
                        for(var k in fms) {
                            if( fms.hasOwnProperty(k)) {
                                sum += fms[k];
                            }
                        }
                    }
                }
                var out = 0;
                for (var idx in scope.item.edges) {
                    if (scope.item.edges.hasOwnProperty(idx)) {
                        var edge = scope.item.edges[idx];
                        if (edge.type === 'mass') {
                            var v = parseFloat(edge.flow)
                            out += v;
                        }
                    }
                }
                scope.maximum = sum;
                scope.used = out;
            }

            scope.$watch('item.edges', function (edges) {
                // watch changes to edges, then find matching connector in dom and change the label
                for (var idx in edges) {
                    if (edges.hasOwnProperty(idx)) {
                        var edge = edges[idx];
                        var id = scope.item.id;
                        var node = GraphUtils.findTarget(scope.contents, edge);
                        var c = jsPlumb.getAllConnections().filter(function (c) {
                            var sourceEndpoint = c.endpoints[0];
                            var targetEndpoint = c.endpoints[1];
                            var sourceId = sourceEndpoint.elementId + sourceEndpoint.anchor.type;
                            var targetId = targetEndpoint.elementId + targetEndpoint.anchor.type;
                            return sourceId === id + edge.source && targetId === edge.target;
                        })[0];
                        c.getOverlay('label').setLabel(NumberFormatter.roundNDecimalPlaces(edge.flow, 1) + 't');
                        calculateMaximum();
                    }
                }
            }, true)
        }
    };
});