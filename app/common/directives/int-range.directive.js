// for each input[type='range'] add attribute 'int-range' to make sure
// that the model is a number, not a string
angular.module('dbfz').directive('intRange', function () {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, elem, attrs, ctrl) {
            //if (attrs.type.toLowerCase() !== 'range') {
            //  return;
            //} //only augment number input!
            ctrl.$parsers.push(function (value) {
                return value ? parseFloat(value) : null;
            });
        }
    };
});