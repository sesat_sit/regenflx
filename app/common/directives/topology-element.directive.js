angular.module('dbfz').directive('topologyElement', function ($timeout) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var content = scope.content;
            //wait until the nodes got rendered via ng-repeat in the html
            //then add endpoints and edges
            $timeout(function () {
                jsPlumb.doWhileSuspended(function () {
                    var id = content.id;
                    jsPlumbDemo.configureEndpoints(content.content_type, id);
                    //gasspeicher and bhkw are draggable again
                    //if(content.content_type != 'gasspeicher' && content.content_type != 'bhkw')
                    jsPlumb.draggable(jsPlumb.getSelector('#' + id), {grid: [10, 10], containment: '#main'});
                    for (var idx in content.edges) {
                        if (content.edges.hasOwnProperty(idx)) {
                            var edge = content.edges[idx];
                            var param = {
                                uuids: [id + edge.source, edge.target],
                                editable: true
                            };
                            var connection = jsPlumb.connect(param);
                            // open flow editor on clicking an edge
                            connection.bind('click', function () {
                                scope.$apply(function () {
                                    scope.editItem(content);
                                });
                            });
                        }
                    }
                    //set style attr of loaded element fixes ie bug
                    //(ie can not interpret angular vars in html, so style={{content.style}} does not work in ie)
                    scope.setStyleOfElement(attrs);
                    //'Cannot read property 'offsetLeft' of null' error risen at initialisation but works fine
                    //try to call in addElement (appangular) same error and does not work
                    scope.makeupGasflow();
                });
            }, 0);
        }
    }
});