/*
 * Directive that uses arrows for manually changing the height of a DOM element
 */
angular.module('dbfz').directive('resizable', function(){
    return {
        scope: true,
        transclude: true,
        templateUrl: 'common/directives/resizable/resizable.directive.html',
        restrict: 'A',
        controller: function($scope){

            //resize drawing array
            $scope.editorStyle = {width: 665,
                height: 400};
            //place increase and decrease drawing area buttons
            $scope.resizeEditorElementStyle = {
                top: $scope.editorStyle.height + 20
            };

            $scope.increaseWidth = function(){
                var width = this.editorStyle.width;
                this.editorStyle.width = (width + 25);
            };

            $scope.decreaseWidth = function(){
                var width = $scope.editorStyle.width;
                if(width > 900){
                    $scope.editorStyle.width = (width - 25);
                }
            };

            $scope.increaseHeight = function(){
                var height = $scope.editorStyle.height;
                $scope.editorStyle.height = (height + 50);
                $scope.resizeEditorElementStyle.top = ($scope.editorStyle.height + 20);
            };

            $scope.decreaseHeight = function(){
                var height = $scope.editorStyle.height;
                if(height > 400){
                    $scope.editorStyle.height = (height - 50);
                    $scope.resizeEditorElementStyle.top = ($scope.editorStyle.height + 20);
                }
            }
        }
    }
});