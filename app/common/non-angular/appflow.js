(function () {
    // this is the paint style for the connecting lines..
    var massConnectorPaintStyle = {
            lineWidth: 4,
            strokeStyle: '#1e8151',
            joinstyle: 'round',
            outlineColor: '#eaedef',
            outlineWidth: 2
        },
        gasConnectorPaintStyle = {
            lineWidth: 5,
            strokeStyle: '#EEEEEE',
            joinstyle: 'round',
            outlineColor: '#F8F8F8',
            outlineWidth: 2
        },
        // .. and this is the hover style.
        connectorHoverStyle = {
            lineWidth: 4,
            strokeStyle: '#5C96BC',
            outlineWidth: 2,
            outlineColor: 'white'
        },
        endpointHoverStyle = {fillStyle: '#5C96BC'},
        // the definition of source endpoints (the small blue ones)
        massSourceEndpoint = {
            endpoint: 'Dot',
            paintStyle: {
                strokeStyle: '#1e8151',
                fillStyle: 'transparent',
                radius: 7,
                lineWidth: 2
            },
            scope: 'mass',
            maxConnections: -1,
            isSource: true,
            connector: ['Flowchart', {curviness: 150}],
            connectorStyle: massConnectorPaintStyle,
            hoverPaintStyle: endpointHoverStyle,
            connectorHoverStyle: connectorHoverStyle,
            dragOptions: {}
        },

        // the definition of target endpoints (will appear when the user drags a connection)
        massTargetEndpoint = {
            endpoint: 'Dot',
            paintStyle: {fillStyle: '#1e8151', radius: 11},
            scope: 'mass',
            hoverPaintStyle: endpointHoverStyle,
            maxConnections: -1,
            dropOptions: {hoverClass: 'hover', activeClass: 'active'},
            isTarget: true
        },
        gasSourceEndpoint = {
            endpoint: 'Dot',
            paintStyle: {
                strokeStyle: '#CCCCCC',
                fillStyle: 'transparent',
                radius: 7,
                lineWidth: 2
            },
            scope: 'gas',
            maxConnections: -1,
            isSource: true,
            connector: ['Flowchart', {curviness: 150}],
            connectorStyle: gasConnectorPaintStyle,
            hoverPaintStyle: endpointHoverStyle,
            connectorHoverStyle: connectorHoverStyle,
            dragOptions: {}
        },

        // the definition of target endpoints (will appear when the user drags a connection)
        gasTargetEndpoint = {
            endpoint: 'Dot',
            paintStyle: {fillStyle: '#CCCCCC', radius: 7},
            scope: 'gas',
            hoverPaintStyle: endpointHoverStyle,
            maxConnections: -1,
            dropOptions: {hoverClass: 'hover', activeClass: 'active'},
            isTarget: true,
            detachable: false
        };

    var _addEndpoints = function (toId, sourceAnchors, targetAnchors) {
        for (var i = 0; i < sourceAnchors.length; i++) {
            var sourceUUID = toId + sourceAnchors[i].text;
            var endpoint = sourceAnchors[i].endpoint;
            jsPlumb.addEndpoint(toId, endpoint, {anchor: sourceAnchors[i].text, uuid: sourceUUID});
        }
        for (var j = 0; j < targetAnchors.length; j++) {
            var targetUUID = toId + targetAnchors[j].text;
            endpoint = targetAnchors[j].endpoint;
            jsPlumb.addEndpoint(toId, endpoint, {anchor: targetAnchors[j].text, uuid: targetUUID});
        }
    };

    window.jsPlumbDemo = {
        isInitialized: false,
        init: function () {

            jsPlumb.importDefaults({
                // default drag options
                DragOptions: {cursor: 'pointer', zIndex: 2000},
                // default to blue at one end and green at the other
                EndpointStyles: [{fillStyle: '#225588'}, {fillStyle: '#558822'}],
                // blue endpoints 7 px; green endpoints 11.
                Endpoints: [['Dot', {radius: 7}], ['Dot', {radius: 11}]],
                // the overlays to decorate each connection with.
                ConnectionOverlays: [
                    ['Arrow', {location: 1}],
                    ['Label', {
                        location: 0.5,
                        id: 'label',
                        cssClass: 'aLabel'
                    }]
                ]
            });

            jsPlumbDemo.isInitialized = true;
        },
        configureEndpoints: function (type, id) {
            var sources, targets;
            switch (type) {
                case 'substrat':
                    sources = [{text: 'RightMiddle', endpoint: massSourceEndpoint}];
                    targets = [];
                    break;
                case 'fermenter':
                    sources = [{text: 'RightMiddle', endpoint: massSourceEndpoint}, {
                        text: 'TopCenter',
                        endpoint: gasSourceEndpoint
                    }];
                    targets = [{text: 'LeftMiddle', endpoint: massTargetEndpoint}];
                    break;
                case 'rest':
                    sources = [];
                    targets = [{text: 'LeftMiddle', endpoint: massTargetEndpoint}];
                    break;
                case 'bhkw':
                    sources = [];
                    targets = [{text: 'LeftMiddle', endpoint: gasTargetEndpoint}];
                    break;
                default:
            }
            _addEndpoints(id, sources, targets);
        }
    };
    jsPlumbDemo.init();

})();