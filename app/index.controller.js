angular.module('dbfz').controller('IndexCtrl', function ($scope, $location, Powerplant, CalculationParameters, GraphUtils) {

    // Starte immer mit dem Anlageneditor, da andernfalls Probleme beim Rendern der Anlage auftauchen (fehlende Kanten).
    $scope.currentTab = '';
    $location.path('');

    // Simuliert eine Änderung im Pfad. Wird über ng-show umgesetzt. Alle 'routeviews' wurden in Direktiven verpackt.
    $scope.changeRouteView = function(path) {
        $location.path(path);
        $scope.currentTab = path;
    };
    $scope.$watch(function (){
        return {stoch: Powerplant.stochiometrie,
                subs: Powerplant.getSubstrate()};
    }, function(_){
        Powerplant.recalculateSums();
    }, true);
    $scope.$watch(function(){return Powerplant.stochiometrie;}, function(s){
        var fx=s.wachstum;
        var fw=s.wasser;
        var c=s.methan;
        s.methanpotential=(1-fx/100+fw/100)/(c/100*16.04+(100-c)/100*44.01)*22.4141*c*10;
    },true);

    // mass flows, use percentages and substrate mass to calculate the mass per element in the graph
    $scope.$watch(function(){ return Powerplant.contents; },
        function(arr){
            function topologicalSort(contents, start, depth) {
                var processed = contents.reduce(function(acc,cur,idx) {
                    if (cur.edges.every(function(edge){
                      return acc.includes(GraphUtils.findTarget(arr, edge));
                    })) {
                        acc.push(cur);
                    }
                    return  acc;
                }, start);
                var nextNames = contents.filter(function(n){ return !processed.includes(n);}),
                    goAgain = nextNames.length && depth <= contents.length;
                var res = goAgain ? topologicalSort(nextNames, processed, depth + 1) : processed;
                res.reverse();
                return res;
            }
            var sorted = topologicalSort(arr, [], 0);
            sorted.forEach(function(elem){
                if(elem.content_type !== 'substrat'){
                    elem.fms = {};
                }
            });
            sorted.forEach(function(item){
                item.edges.forEach(function(edge){
                    if(edge.type==='mass') {
                        var target = GraphUtils.findTarget(sorted, edge);
                        if (target) {
                            // console.log(edge.sourceId,"->",target.id,"flow=",edge.flow,"fm before=",target.fm);
                            var fms = target.fms = target.fms || {};
                            fms[edge.sourceId]=edge.flow; // propagate outgoing flow to target node
                            // console.log("fm after=",target.fm);
                        }
                    }
                });
            });
            $scope.fermenter = Powerplant.getFermenter();
            $scope.gasProFermenter = CalculationParameters.calcProduction(CalculationParameters.createParametersForCalculations());
        }, true);
});