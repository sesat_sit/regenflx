# Initiale Einrichtung der Entwicklungumgebung

[NPM](http://nodejs.org/download/) ist der *Node.js package manager*. 
Dieses Werkzeug verwaltet die folgenden Werkzeuge: `gulp`, `bower`. 
Node.js muss entsprechend installiert sein.

[Bower](http://bower.io/) verwaltet Abhängigkeiten zu öffentlichen Artefakten wie JQuery und AngularJS.

Installation:

    npm install -g bower

[Gulp](https://gulpjs.com/) ist das verwendete Build-Tool.

Installation:

    npm install -g gulp

# Bauen des Projektes

Bauen des Projektes zur Entwicklungszeit:

    // Installiere Plugins für 'gulp' unter 'node_modules'.
    npm install 
    
    // Installiere externe Artefakte unter 'app/bower_components'.
    bower install
    
    // Starte lokalen Webserver unter 'localhost:8888'.
    gulp webserver

Bauen des Standes für den produktiven Einsatz:

    // Erstelle ein Produktiv-Build unter 'app/dist'.
    gulp build
    
    // Betrachte das Produktiv-Build mit einem lokalen Webserver unter 'localhost:9000'.
    gulp webserver:dist